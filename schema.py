from pydantic import UUID4, BaseModel
from datetime import date
from typing import List, Optional

class News(BaseModel):
    title:str
    content:str
    created_at:Optional[date]
    is_deleted:Optional[bool]

    class Config:
        orm_mode=True


class Report(BaseModel):
    id:UUID4 
    title:str
    is_deleted:Optional[bool]
    chartstype:int
    datatype:int
    columns:str

    class Config:
        orm_mode=True



class Group(BaseModel):
    id:UUID4 
    title:str
    reports:List[Report]

    class Config:
        orm_mode=True


class Category(BaseModel):
    id:UUID4 
    title:str
    groups:List[Group]
    
    class Config:
        orm_mode=True
<<<<<<< HEAD


class ReportResult(BaseModel):
    query:str
=======
#TODO: Написать схему которая вызывает данные из таблицы категории связывается по id с таблицей групп и связывается по id с таблицей отчетов
#TODO: id каждой из таблиц sys_guid (Oracle)  из-за чего не получается связать по связи многие ко многим таблицы категорий групп и отчетов
>>>>>>> a3cc79243d24f53fb150712447fe28b44e109622
