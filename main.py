from fastapi import FastAPI
from db import models
from db.database import engine
from routers import icportal


app = FastAPI()
app.include_router(icportal.router)

models.Base.metadata.create_all(engine)