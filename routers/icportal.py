from fastapi import APIRouter, Depends
from pydantic import UUID4
from schema import News, Group, Category, News, Report, ReportResult
from sqlalchemy.orm import Session
from db.database import get_db
from typing import List
# from db import db_icportal
from db import models


router = APIRouter(
    prefix = '/ic-portal/api',
    tags = ['ic-portal']
)


@router.get('/reports', response_model=List[Report])
def report_all(db: Session = Depends(get_db)):
    reports = db.query(models.Report).all()
    return reports


@router.get('/report_categories', response_model=List[Category])
def category_all(db: Session = Depends(get_db)):
    categories = db.query(models.ReportCategory).all()
    return categories


@router.get('/news', response_model=List[News])
def get_news(db: Session = Depends(get_db)):
    news = db.query(models.News).all()
    return news


<<<<<<< HEAD
@router.get('/report{id}/result', response_model=List[ReportResult])
def report_result(id:UUID4, db:Session=Depends(get_db)):
    report = db.query(models.Report).filter(models.Report.id == id).one()
    return report
=======
@router.get('/report/{id}/result', response_model=[Category])
def category_all(id:str, db:Session = Depends(get_db)):
    report = db.query(models.Report).filter(models.Report.id == id).first()
    # Код ниже выдаст ошибку из-за разных диалектов SQLite и Oracle в поле query
    return db.execute(report.query).fetchall()
>>>>>>> a3cc79243d24f53fb150712447fe28b44e109622
