"""
Загрузка отношений. Подробнее 
https://docs.sqlalchemy.org/en/14/orm/loading_relationships.html
"""
from pprint import pprint
from sqlalchemy.orm import joinedload, Session
import pprint

from schema import *
from db.models import *
from db.database import engine

# # get group with id 1
# with Session(bind=engine) as session:
#     b1 = session.query(ReportGroup).where(ReportGroup.id == 1).one()
#     print(b1.title)

# # get each report of this group
# with Session(bind=engine) as session:
#     b1 = session.query(ReportGroup).\
#         options(joinedload(ReportGroup.reports)).\
#             where(ReportGroup.id == 1).one()
#     print(b1.title)

with Session(bind=engine) as session:
    b1 = session.query(ReportGroup).\
        options(joinedload(ReportGroup.reports)).\
            options(joinedload(ReportGroup.categories))\
                .first()    

b1_schema = GroupSchema.from_orm(b1)
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(b1_schema.json())

from db.database import engine 
from sqlalchemy.orm import Session             
import db.models as mod
from schema import Category, Group, Report

with Session(bind=engine) as session:
    for report in session.query(mod.ReportCategory).all():
        print(report.id, report.title, report.groups)
        for groups in report.groups: 
            print(groups.id, groups.title, groups.reports)
            for g_report in groups.reports:
                print(g_report.id, g_report.title)