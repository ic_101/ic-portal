"""
Создание БД и отношений в БД "Многие ко могим"
https://docs.sqlalchemy.org/en/14/orm/basic_relationships.html#many-to-many
"""
from sqlalchemy.orm import relationship
from datetime import datetime
from db.database import Base
import sqlalchemy as sa
from uuid import uuid4
from sqlalchemy_utils import UUIDType

class News(Base) : 
    __tablename__ = 'site_news'
    id =  sa.Column('id', UUIDType, primary_key=True, default=uuid4, index=True)
    title = sa.Column('title', sa.String)
    content = sa.Column('content', sa.String)
    created_at = sa.Column('created_at', sa.Date, default=datetime.now())
    is_deleted = sa.Column('is_deleted', sa.Boolean, default=False)


site_report_group = sa.Table('site_report_group', Base.metadata,
    sa.Column('group_id', sa.ForeignKey('site_report_groups.id'), primary_key=True),
    sa.Column('categories_id', sa.ForeignKey('site_report_categories.id'), primary_key=True)
)


site_report_category_group = sa.Table('site_report_category_group', Base.metadata,
    sa.Column('group_id', sa.ForeignKey('site_report_groups.id'), primary_key=True),
    sa.Column('reports_id', sa.ForeignKey('site_reports.id'), primary_key=True)
)


class ReportGroup(Base):
    __tablename__ = 'site_report_groups'
    id =  sa.Column('id', UUIDType, primary_key=True, default=uuid4, index=True)
    title = sa.Column('title', sa.String)
    categories = relationship('ReportCategory', secondary='site_report_group', back_populates='groups')
    reports = relationship('Report', secondary='site_report_category_group', back_populates='groups')


class ReportCategory(Base):
    __tablename__ = 'site_report_categories'
    id =  sa.Column('id', UUIDType, primary_key=True, default=uuid4, index=True)
    title = sa.Column('title', sa.String)
    groups = relationship('ReportGroup', secondary='site_report_group', back_populates='categories')


class Report(Base):
    __tablename__= 'site_reports'
    id =  sa.Column('id', UUIDType, primary_key=True, default=uuid4, index=True)
    title = sa.Column('title', sa.String)
    query = sa.Column('query', sa.CLOB)
    is_deleted = sa.Column('is_deleted', sa.Boolean, default=False)
    chartstype = sa.Column('charts_type', sa.Integer)
    datatype = sa.Column('data_type', sa.Integer)
    columns = sa.Column('columns', sa.String)
    groups = relationship('ReportGroup', secondary='site_report_category_group', back_populates='reports')