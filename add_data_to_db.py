from turtle import title
from db.database import engine
# from db.models import ReportCategory, ReportGroup, Report
from db.models import ReportCategory, ReportGroup, Report, News

from sqlalchemy.orm import Session

with Session(bind=engine) as session:
    report1=Report(# По направлению деят-ти ИАЗ, Сверки
        title='Сверка №42',
        query='select t.kod_upmo, extract(year from t.date_sver) as year_sv, \
            extract(month from t.date_sver) as month_sv, count(1) as kol \
            from sverka.sv_42 t group by t.kod_upmo, \
            extract(year from t.date_sver), extract(month from t.date_sver)',
        chartstype=1,
        datatype=0,
        columns=' '
    )
    report2=Report(#По направлению деят-ти УУПиПДН, Сверки
        title='Сверка №59',
        query='select t.kod_upmo, extract(year from t.date_sver) as year_sv, \
            extract(month from t.date_sver) as month_sv, count(1) as kol \
                from sverka.sv_59 t\
                    group by t.kod_upmo, extract(year from t.date_sver), \
                        extract(month from t.date_sver)',
        chartstype=1,
        datatype=0,
        columns=' '
    )
    report3=Report(# По вопросам миграции, Сверки
        title='Сверка №58',
        query='select t.kod_upmo, extract(year from t.date_sver) as year_sv, \
            extract(month from t.date_sver) as month_sv, count(1) as kol \
                from sverka.sv_58 t \
                    group by t.kod_upmo, extract(year from t.date_sver), \
                        extract(month from t.date_sver)',
        chartstype=1,
        datatype=0,
        columns=' '
    )
    report4=Report(# По направлению деят-ти ИАЗ, Сверки
        title='Сверка №42 круговая',
        query='select t.kod_upmo, count(1) as kol\
            from sverka.sv_42 t\
                where t.date_sver >= add_months(trunc(sysdate,"MM")+4,-1) \
                    group by t.kod_upmo \
                        union \
                            select 99 as kod_upmo, count(1) as kol \
                                from sverka.sv_42 t\
                                    where t.date_sver >= add_months(trunc(sysdate,"MM")+4,-1)',
        chartstype=2,
        datatype=0,
        columns=' '
    )
    group1=ReportGroup(title='По направлению деят-ти ИАЗ')
    group2=ReportGroup(title='По направлению деят-ти УУПиПДН')
    group3=ReportGroup(title='По вопросам миграции')
    group4=ReportGroup(title='Розыск АМТС')

    categories1=ReportCategory(title='УРД')
    categories2=ReportCategory(title='Формирование учетов')
    categories3=ReportCategory(title='Работа с ИБД')
    categories4=ReportCategory(title='Сверки')

    group1.reports=[report1, report4]
    group2.reports=[report2]
    group3.reports=[report3]
    group1.categories=[categories4]
    group2.categories=[categories4]
    group3.categories=[categories4]
    group4.categories=[categories2]

    news1=News(title='ИБД-Ф', content='"')
    news2=News(title='Списки личного состава', content='"')
    news3=News(title='Дактилоскопические карты нового образца', content='"')
    news4=News(title='ИБД-Р', content='"')
    news5=News(title='COVID-19', content='"')
    news6=News(title='О направлении алгоритма', content='"')
    news7=News(title='Отчет по "тишине" за 10 месяцев ', content='"')

    session.add_all([group1, group2, categories1, categories2, categories3,
                  news1, news2, news3, news4, news5, news6, news7
                  ])
    session.commit()
    

    